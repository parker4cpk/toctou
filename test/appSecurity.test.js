const request = require('supertest');
const { app, transfer } = require('./app');

describe('security', () => {

    it('Request for negative amount, should return 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: -500});
        expect(res.statusCode).toEqual(400);
    });

    it('TOCTOU check 505 use 2000, should succesffuly process 505', async () => {
        const payload = {
            checked: 0,
            toString() {
                if (this.checked >= 1) {
                    return 2000
                }
                this.checked = 1
                return 505
            }
        };
        expect(transfer(1000, payload)).toEqual({ balance: 495, transferred: 505 });
    });
});
