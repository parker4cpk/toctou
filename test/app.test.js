const request = require('supertest');
const { app, transfer } = require('./app');

describe('usability', () => {

    it('Requst to transfer 500, should return successfully', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 500});
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Successfully transferred: 500. Your balance: 500');
    });

    it('Requst to transfer 1001, should fail with 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 1001});
        expect(res.statusCode).toEqual(400);
    });

    it('when request /status, should return 200', async () => {
        const res = await request(app)
            .get('/status');
        expect(res.statusCode).toEqual(200);
    });


});
