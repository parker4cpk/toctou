module.exports = class Money {
    constructor(value) {
        this.validate(value);
        this.value = value;
        Object.freeze(this);
    }

    static fromString(value) {
        value = parseInt(value);
        return new Money(value);
    }

    static fromNumber(value) {
        return new Money(value);
    }

    validate(value) {
        if (value === null || value === undefined)
            throw new TypeError();

        if (Number.isNaN(value) || !Number.isInteger(value))
            throw new TypeError();

        if (value < 0 || value >= Number.MAX_SAFE_INTEGER)
            throw new RangeError();
    }
}
