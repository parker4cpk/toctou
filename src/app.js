'use strict';

// requirements
const express = require('express');
const Money = require('./money');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send
app.get('/', (req, res) => {
    let balance = 1000;
    let amount = req.query.amount;
    let transferred;

    try {
        const result = transfer(balance, amount);

        balance = result.balance;
        transferred = result.transferred;
    } catch (err) {
        if (err instanceof TypeError || err instanceof RangeError) {
            res.status(400).end('Invalid input.');
            return;
        } else {
            throw err;
        }
    }

    if (balance !== undefined || transferred !== undefined) {
        res.status(200).end('Successfully transferred: ' + transferred + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// Transfer amount service
var transfer = (passedBalance, passedAmount) => {
    var transferred = Money.fromNumber(0);

    // Ideally the conversion/validation should occur as soon as we receive the request, but that would break the test.
    let balance = Money.fromNumber(passedBalance);
    let amount = Money.fromString(passedAmount);

    if (
        amount.value >= 0 &&
        amount.value <= balance.value
    ) {
        balance = Money.fromNumber(balance.value - amount.value);
        transferred = Money.fromNumber(transferred.value + amount.value);
        return { balance: balance.value, transferred: transferred.value };
    } else
        return { undefined, undefined };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
